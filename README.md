# Ops To Dev

What to bring, and what to leave behind.

This is a talk I will be giving at DevOpsDays London 2017; this repository holds the write-up & source.

## Recordings

You can find a write-up of this talk on my blog: https://blag.esotericsystems.at/articles/ops2dev/
and you can find a video recording here: https://www.youtube.com/watch?v=qhqw88oU6kE

## License

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="https://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://gitlab.com/i.galic/ops2dev">
    <span property="dct:title">Igor Galić</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Ops To Dev</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="DE" about="https://gitlab.com/i.galic/ops2dev">
  Germany</span>.
</p>
